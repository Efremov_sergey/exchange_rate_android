package com.esv.exchange_rate_android.domain.use_cases

import com.esv.exchange_rate_android.domain.repository.CurrencyRepository

class GetCurrencyUseCase(private val currencyRepository: CurrencyRepository) {
    fun execute() = currencyRepository.loadCurrency()
}