package com.esv.exchange_rate_android.domain.entities

data class Currency(
    val id: Int,
    val title: String,
    val code: String
)
