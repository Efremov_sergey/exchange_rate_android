package com.esv.exchange_rate_android.domain.repository

import com.esv.exchange_rate_android.domain.entities.Currency
import com.esv.exchange_rate_android.domain.entities.DailyResult
import com.esv.exchange_rate_android.domain.entities.ExchangeRate
import kotlinx.coroutines.flow.Flow

interface ExchangeRateRepository {
    suspend fun getDailyResult(): Flow<DailyResult>

    fun updateExchangeRate(rateList: List<ExchangeRate>)

    fun saveToDB(from: Currency, to: Currency, rate: Float)
}