package com.esv.exchange_rate_android.domain.use_cases.currency

import com.esv.exchange_rate_android.domain.entities.Currency
import com.esv.exchange_rate_android.domain.entities.ExchangeRate
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single

interface CurrencyUseCaseInterface {
    fun loadFromDB(): Flow<List<Currency>>
    fun loadFromNetwork(): Flow<List<Currency>>
}

// TODO: think about correct naming of this class and logic
class CurrencyUseCase {
    var currencyList: Flow<Currency> = flowOf()
    val exchangeRate: Flow<ExchangeRate> = flowOf()
    var rate = flowOf<Float>()

    fun loadFromDB(): Flow<List<Currency>> {
        TODO("Not yet implemented")
    }

    fun loadFromNetwork(): Flow<List<Currency>> {
        TODO("Not yet implemented")
    }

    suspend fun updateExchangeRate(from: Currency?, to: Currency?) {
        if (exchangeRate.single().fromCurrency != from || exchangeRate.single().toCurrency != to) {
            // TODO: get exchangeRate from DB
        }
    }

    fun calculateRate(rate: ExchangeRate, count: Float) {
        this.rate = flowOf(rate.rate * count)
    }
}