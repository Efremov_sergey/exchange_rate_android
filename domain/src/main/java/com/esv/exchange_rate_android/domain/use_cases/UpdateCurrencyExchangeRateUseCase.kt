package com.esv.exchange_rate_android.domain.use_cases

import com.esv.exchange_rate_android.domain.repository.ExchangeRateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class UpdateCurrencyExchangeRateUseCase(private val exchangeRateRepository: ExchangeRateRepository) {
    fun execute(): Flow<Boolean> = flow {
        exchangeRateRepository.getDailyResult().collect { result ->
            exchangeRateRepository.updateExchangeRate(result.rateList)
        }
    }
}