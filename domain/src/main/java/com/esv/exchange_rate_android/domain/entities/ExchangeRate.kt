package com.esv.exchange_rate_android.domain.entities

data class ExchangeRate(
    val fromCurrency: Currency,
    val toCurrency: Currency,
    val rate: Float
)