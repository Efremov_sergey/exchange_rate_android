package com.esv.exchange_rate_android.domain.entities

import java.util.*

data class DailyResult(
    val date: Date?,
    val rateList: List<ExchangeRate>
)
