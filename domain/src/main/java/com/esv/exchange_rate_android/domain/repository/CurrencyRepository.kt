package com.esv.exchange_rate_android.domain.repository

import com.esv.exchange_rate_android.domain.entities.Currency
import kotlinx.coroutines.flow.Flow

interface CurrencyRepository {
    fun loadCurrency(): Flow<List<Currency>>
}