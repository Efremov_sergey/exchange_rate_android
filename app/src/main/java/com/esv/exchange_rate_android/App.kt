package com.esv.exchange_rate_android

import android.app.Application
import com.esv.exchange_rate_android.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(listOf(viewModelModule, repositoryModule, netModule, apiModule, databaseModule, useCasesModules))
        }
    }
}