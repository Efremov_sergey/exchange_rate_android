package com.esv.exchange_rate_android.presentation.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.esv.exchange_rate_android.domain.entities.Currency
import com.esv.exchange_rate_android.domain.use_cases.GetCurrencyUseCase
import com.esv.exchange_rate_android.domain.use_cases.UpdateCurrencyExchangeRateUseCase

class MainViewModel(
    private val getCurrencyUseCase: GetCurrencyUseCase,
    private val updateCurrencyExchangeRateUseCase: UpdateCurrencyExchangeRateUseCase
) : ViewModel() {
    val currencyList = getCurrencyUseCase.execute().asLiveData()
    val updateData = updateCurrencyExchangeRateUseCase.execute().asLiveData()

    // TODO: add binding
    val fromCurrency = MutableLiveData<Currency?>()
    val toCurrency = MutableLiveData<Currency?>()
//    val resultCost = useCase.rate

//    val exchangeRate: LiveData<ExchangeRate> = useCase.exchangeRate.asLiveData()
    // TODO: add binding
    val cost = MutableLiveData<Float>()

    // TODO: add action on select currency
    fun selectCurrency(isFrom: Boolean, currency: Currency) {
        if (isFrom) {
            fromCurrency.value = currency
        } else {
            toCurrency.value = currency
        }

        updateExchangeRate()
    }

    // TODO: add action to button calculate
    fun calculate() {
//        if (exchangeRate.value != null && cost.value != null) {
//            useCase.calculateRate(exchangeRate.value!!, cost.value!!)
//        }
    }

    private fun updateExchangeRate() {
//        CoroutineScope(Dispatchers.IO).launch {
//            useCase.updateExchangeRate(fromCurrency.value, toCurrency.value)
//        }
    }
}