package com.esv.exchange_rate_android.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.esv.exchange_rate_android.R
import com.esv.exchange_rate_android.presentation.ui.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}