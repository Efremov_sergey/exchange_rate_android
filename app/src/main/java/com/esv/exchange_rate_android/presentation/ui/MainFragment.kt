package com.esv.exchange_rate_android.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.esv.exchange_rate_android.R
import com.esv.exchange_rate_android.databinding.MainFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel by viewModel<MainViewModel>()
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.main_fragment, container, false
        )

        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_selectable_list_item,
            mutableListOf<String>()
        )

        binding.spinnerFromCurrency.adapter = adapter
        binding.spinnerFromCurrency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        viewModel.currencyList.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                adapter.clear()
                adapter.addAll(it.map { it.title })
            }
        }

        viewModel.updateData.observe(viewLifecycleOwner) {

        }

        return binding.root
    }

    override fun onDestroyView() {
        binding.spinnerFromCurrency.onItemSelectedListener = null
        super.onDestroyView()
    }
}