package com.esv.exchange_rate_android.di

import com.esv.exchange_rate_android.domain.use_cases.GetCurrencyUseCase
import com.esv.exchange_rate_android.domain.use_cases.UpdateCurrencyExchangeRateUseCase
import com.esv.exchange_rate_android.domain.use_cases.currency.CurrencyUseCase
import com.esv.exchange_rate_android.presentation.ui.MainViewModel
import com.esv.excharge_rate_android.data.remote.api.Api
import com.esv.excharge_rate_android.data.repository.CurrencyRepositoryImpl
import com.esv.excharge_rate_android.data.repository.ExchangeRateRepositoryImpl
import com.esv.excharge_rate_android.data.storage.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get(), get()) }
}

val apiModule = module {
    single { Api.getInstance() }
}

val netModule = module {

}

val databaseModule = module {
    single { AppDatabase.getInstance(androidContext()) }
    single { AppDatabase.getInstance(androidContext()).currencyDao }
    single { AppDatabase.getInstance(androidContext()).exchangeRateDao }
    single { AppDatabase.getInstance(androidContext()).userSettingsDao }
}

val repositoryModule = module {
    single { CurrencyRepositoryImpl(get()) }
}

val useCasesModules = module {
    single { GetCurrencyUseCase(CurrencyRepositoryImpl(get())) }
    single { UpdateCurrencyExchangeRateUseCase(ExchangeRateRepositoryImpl(get(), get(), get())) }
    single { CurrencyUseCase() }
}