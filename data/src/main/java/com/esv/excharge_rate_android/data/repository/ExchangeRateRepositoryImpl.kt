package com.esv.excharge_rate_android.data.repository

import com.esv.exchange_rate_android.domain.entities.DailyResult
import com.esv.exchange_rate_android.domain.repository.ExchangeRateRepository
import com.esv.excharge_rate_android.data.converter.DataConverterImpl.convertApiResultToExchangeRate
import com.esv.excharge_rate_android.data.converter.DataConverterImpl.convertCurrencyToData
import com.esv.excharge_rate_android.data.converter.DataConverterImpl.exchangeRateFromValue
import com.esv.excharge_rate_android.data.converter.DataConverterImpl.toListExchangeRateAndReverse
import com.esv.excharge_rate_android.data.remote.api.Api
import com.esv.excharge_rate_android.data.storage.dao.ExchangeRateDao
import com.esv.excharge_rate_android.data.storage.model.ExchangeRate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

typealias ExchangeRateDomain = com.esv.exchange_rate_android.domain.entities.ExchangeRate

// TODO: replace AppDatabase to DAO, because repo shouldn't have access to another DAO
class ExchangeRateRepositoryImpl(
    private val currencyRepositoryImpl: CurrencyRepositoryImpl,
    private val exchangeRateDao: ExchangeRateDao,
    private val api: Api
) : ExchangeRateRepository {

    var format: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US)

    override suspend fun getDailyResult(): Flow<DailyResult> = flow {
        currencyRepositoryImpl.loadCurrency().collect { currencyList ->
            val data = api.getDaily()
            emit(
                DailyResult(
                    format.parse(data.date),
                    data.convertApiResultToExchangeRate(currencyList)
                )
            )
        }
    }.flowOn(Dispatchers.IO)

    override fun updateExchangeRate(rateList: List<ExchangeRateDomain>) {
        val resultList: MutableList<ExchangeRate> = rateList.toListExchangeRateAndReverse()

        rateList.forEach { from ->
            rateList.forEach { to ->
                if (to.fromCurrency.id != from.fromCurrency.id)
                    resultList.add(
                        exchangeRateFromValue(
                            from.fromCurrency, to.fromCurrency, from.rate / to.rate
                        )
                    )
            }
        }

        CoroutineScope(Dispatchers.IO).launch {
            exchangeRateDao.insertAll(resultList)
        }
    }

    override fun saveToDB(from: CurrencyDomain, to: CurrencyDomain, rate: Float) =
        exchangeRateDao.insert(
            ExchangeRate(
                first = convertCurrencyToData(from),
                second = convertCurrencyToData(to),
                rate = rate
            )
        )
}