package com.esv.excharge_rate_android.data.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.esv.excharge_rate_android.data.storage.model.ExchangeRate

@Dao
interface ExchangeRateDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(rate: ExchangeRate)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(rateList: List<ExchangeRate>)
}