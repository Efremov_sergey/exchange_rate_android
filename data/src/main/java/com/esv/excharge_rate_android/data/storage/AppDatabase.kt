package com.esv.excharge_rate_android.data.storage

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.esv.excharge_rate_android.data.storage.dao.CurrencyDao
import com.esv.excharge_rate_android.data.storage.dao.ExchangeRateDao
import com.esv.excharge_rate_android.data.storage.dao.UserSettingsDao
import com.esv.excharge_rate_android.data.storage.model.Currency
import com.esv.excharge_rate_android.data.storage.model.ExchangeRate
import com.esv.excharge_rate_android.data.storage.model.UserSettings
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [Currency::class, ExchangeRate::class, UserSettings::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract val currencyDao: CurrencyDao
    abstract val exchangeRateDao: ExchangeRateDao
    abstract val userSettingsDao: UserSettingsDao

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDataSource(context).also { instance = it }
            }
        }

        private fun buildDataSource(context: Context): AppDatabase = Room.databaseBuilder(
            context, AppDatabase::class.java, "er_database")
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    CoroutineScope(Dispatchers.Default).launch {
                        getInstance(context).currencyDao.insertAll(listOf(
                            Currency(0, "Рубли", "RUB"),
                            Currency(1, "Доллары США", "USD"),
                            Currency(2, "Евро", "EUR"),
                            Currency(3, "Британский фунт", "GBP"),
                            Currency(4, "Швейцарский франк", "CHF"),
                            Currency(5, "Китайский юань", "CNY")
                        ))
                    }
                }
            })
            .build()
    }
}