package com.esv.excharge_rate_android.data.remote.model

import com.google.gson.annotations.SerializedName

data class ExchangeRateToRub(
    @SerializedName("CharCode")
    val charCode: String,
    @SerializedName("Value")
    val value: Float
)
