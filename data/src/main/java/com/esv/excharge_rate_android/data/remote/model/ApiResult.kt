package com.esv.excharge_rate_android.data.remote.model

import com.google.gson.annotations.SerializedName

data class ApiResult(
    @SerializedName("Date")
    val date: String,
    @SerializedName("Valute")
    val valute: Map<String, ExchangeRateToRub>
)
