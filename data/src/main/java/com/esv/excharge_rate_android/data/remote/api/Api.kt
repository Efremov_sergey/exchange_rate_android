package com.esv.excharge_rate_android.data.remote.api

import com.esv.excharge_rate_android.data.remote.model.ApiResult
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {
    @GET(DAILY)
    suspend fun getDaily(): ApiResult

    companion object {
        private const val BASE_URL = " https://www.cbr-xml-daily.ru/"
        private const val DAILY = "daily_json.js"

        var retrofitService: Api? = null

        fun getInstance() : Api {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(Api::class.java)
            }
            return retrofitService!!
        }
    }
}
