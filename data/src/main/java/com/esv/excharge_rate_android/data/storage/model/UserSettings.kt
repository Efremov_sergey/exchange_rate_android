package com.esv.excharge_rate_android.data.storage.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_settings")
data class UserSettings(
    @PrimaryKey
    val id: Int,
    @Embedded val selectedCurrency: Currency
)
