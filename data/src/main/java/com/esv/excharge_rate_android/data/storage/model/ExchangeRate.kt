package com.esv.excharge_rate_android.data.storage.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "exchange_rate")
data class ExchangeRate(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "rate_id")
    val rateId: Int = 0,
    @Embedded(prefix = "first_") val first: Currency,
    @Embedded(prefix = "second_") val second: Currency,
    val rate: Float
)