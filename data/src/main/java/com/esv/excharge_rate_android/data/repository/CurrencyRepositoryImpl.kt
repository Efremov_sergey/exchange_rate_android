package com.esv.excharge_rate_android.data.repository

import com.esv.exchange_rate_android.domain.repository.CurrencyRepository
import com.esv.excharge_rate_android.data.converter.DataConverterImpl.convertCurrencyListToDomainList
import com.esv.excharge_rate_android.data.storage.dao.CurrencyDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

typealias CurrencyDomain = com.esv.exchange_rate_android.domain.entities.Currency

class CurrencyRepositoryImpl(
    private val currencyDao: CurrencyDao
): CurrencyRepository {
    override fun loadCurrency(): Flow<List<CurrencyDomain>> = currencyDao.getAll().map { list ->
        convertCurrencyListToDomainList(list)
    }
}