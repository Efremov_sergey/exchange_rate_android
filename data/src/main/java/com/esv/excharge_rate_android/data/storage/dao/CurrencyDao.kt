package com.esv.excharge_rate_android.data.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.esv.excharge_rate_android.data.storage.model.Currency
import kotlinx.coroutines.flow.Flow

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currency")
    fun getAll(): Flow<List<Currency>>

    @Insert
    fun insertAll(list: List<Currency>)
}