package com.esv.excharge_rate_android.data.converter

import com.esv.excharge_rate_android.data.remote.model.ApiResult
import com.esv.excharge_rate_android.data.storage.model.Currency
import com.esv.excharge_rate_android.data.storage.model.ExchangeRate

typealias DomainCurrency = com.esv.exchange_rate_android.domain.entities.Currency
typealias DomainExchangeRate = com.esv.exchange_rate_android.domain.entities.ExchangeRate

interface DataConverter {
    fun convertCurrencyToDomain(currency: Currency): DomainCurrency
    fun convertCurrencyListToDomainList(currency: List<Currency>): List<DomainCurrency>
    fun convertCurrencyToData(currency: DomainCurrency): Currency
    fun ApiResult.convertApiResultToExchangeRate(currencyList: List<DomainCurrency>): List<DomainExchangeRate>
    fun List<DomainExchangeRate>.toListExchangeRateAndReverse(): MutableList<ExchangeRate>
    fun DomainExchangeRate.toExchangeRate(): ExchangeRate
    fun DomainExchangeRate.toExchangeRateReverse(): ExchangeRate
    fun exchangeRateFromValue(from: DomainCurrency, to: DomainCurrency, rate: Float): ExchangeRate
}

object DataConverterImpl : DataConverter {
    override fun convertCurrencyToDomain(currency: Currency): DomainCurrency = DomainCurrency(
        currency.currencyId,
        currency.title,
        currency.code
    )

    override fun convertCurrencyListToDomainList(currency: List<Currency>): List<DomainCurrency> {
        return currency.map {
            convertCurrencyToDomain(it)
        }
    }

    override fun convertCurrencyToData(currency: DomainCurrency): Currency = Currency(
        currency.id,
        currency.title,
        currency.code
    )

    override fun ApiResult.convertApiResultToExchangeRate(currencyList: List<DomainCurrency>): List<DomainExchangeRate> =
        this.valute.values.filter { rate ->
            currencyList.find {
                it.code == rate.charCode
            } != null
        }.map { rate ->
            val currency = currencyList.first {
                it.code == rate.charCode
            }
            val ru = currencyList.first {
                it.code == "RUB"
            }
            DomainExchangeRate(
                currency,
                ru,
                rate.value
            )
        }

    override fun List<DomainExchangeRate>.toListExchangeRateAndReverse(): MutableList<ExchangeRate> {
        val list = this.map {
            it.toExchangeRate()
        }.toMutableList()
        list.addAll(this.map {
            it.toExchangeRateReverse()
        })
        return list
    }

    override fun DomainExchangeRate.toExchangeRate(): ExchangeRate = ExchangeRate(
        first = convertCurrencyToData(this.fromCurrency),
        second = convertCurrencyToData(this.toCurrency),
        rate = this.rate
    )

    override fun DomainExchangeRate.toExchangeRateReverse() = ExchangeRate(
        first = convertCurrencyToData(this.toCurrency),
        second = convertCurrencyToData(this.fromCurrency),
        rate = 1 / this.rate
    )

    override fun exchangeRateFromValue(from: DomainCurrency, to: DomainCurrency, rate: Float): ExchangeRate = ExchangeRate(
        first = convertCurrencyToData(from),
        second = convertCurrencyToData(to),
        rate = rate
    )
}