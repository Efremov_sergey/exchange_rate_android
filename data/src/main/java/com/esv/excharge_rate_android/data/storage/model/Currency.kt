package com.esv.excharge_rate_android.data.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency")
data class Currency(
    @PrimaryKey @ColumnInfo(name = "currency_id")
    val currencyId: Int,
    val title: String,
    val code: String
)